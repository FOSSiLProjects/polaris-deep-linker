<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
	<link rel="canonical" href="https://playingaround.com/">
	<meta name="robots" content="index, follow">
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?877">
	<link rel="stylesheet" type="text/css" href="style.css?7286">
	<link rel="stylesheet" type="text/css" href="./css/all.min.css">
	
    <title>Home</title>


    
<!-- Analytics -->
 
<!-- Analytics END -->
    
</head>
<body>

<!-- Preloader -->
<div id="page-loading-blocs-notifaction" class="page-preloader"></div>
<!-- Preloader END -->


<!-- Main container -->
<div class="page-container">
    
<!-- bloc-0 -->
<div class="bloc" id="bloc-0">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col-12">
				<img src="img/library-4785580_1920.jpg" class="img-fluid mx-auto d-block" alt="library 4785580_1920" />
			</div>
			<div class="col-12">
				<h1 class="mg-md text-lg-center text-md-center text-sm-center text-center tc-black">
					<strong>Polaris Deep Linker</strong>
				</h1>
				<p class="text-lg-center text-md-center text-sm-center text-center">
					Create a persistent link to materials and results list in your Polaris PowerPAC.
				</p>
			</div>
		</div>
	</div>
</div>
<!-- bloc-0 END -->

<!-- bloc-0 -->
<div class="bloc bgc-platinum l-bloc " id="bloc-0">
	<div class="container bloc-lg bloc-md-lg">
		<div class="row l-bloc">
			<div class="col">
				<form id="form_1" data-form-type="blocs-form" novalidate data-success-msg="Your message has been sent." data-fail-msg="Sorry it seems that our mail server is not responding, Sorry for the inconvenience!">
					<div class="form-group">
						<label>
							Base URL
						</label><span class="far fa-question-circle icon-sm" data-placement="right" data-toggle="tooltip" title="The base URL of your PowerPAC"></span>
						<input id="url" class="form-control" required placeholder="https://catalog.mylibrary.org" />
					</div>
					<div class="form-group">
						<label>
							CTX<br>
						</label><span class="far fa-question-circle icon-sm" data-placement="right" data-toggle="tooltip" title="The CTX number designates a branch and display langauge for your PAC."></span>
						<input id="ctx" class="form-control" required placeholder="3.1033.0.0.6" />
					</div>
					<div class="form-group">
						<label>
							Keyword(s)<br>
						</label><span class="far fa-question-circle icon-sm" data-placement="right" data-toggle="tooltip" title="Search by one or more keywords"></span>
						<input id="keywords" class="form-control" placeholder="Batgirl comics" />
					</div>
					<div class="form-group">
						<label>
							Title<br>
						</label><span class="far fa-question-circle icon-sm" data-placement="right" data-toggle="tooltip" title="Search by title"></span>
						<input id="title" class="form-control" placeholder="The Oracle Code" />
					</div>
					<div class="form-group">
						<label>
							Author<br>
						</label><span class="far fa-question-circle icon-sm" data-placement="right" data-toggle="tooltip" title="Search by author"></span>
						<input id="author" class="form-control" placeholder="Gail Simone" />
					</div>
					<div class="form-group">
						<label>
							Subject Heading<br>
						</label><span class="far fa-question-circle icon-sm" data-placement="right" data-toggle="tooltip" title="Search by subject heading"></span>
						<input id="subject" class="form-control" placeholder="Graphic Novels" />
					</div>
					<div class="form-group">
						<label>
							Language<br>
						</label><span class="far fa-question-circle icon-sm" data-placement="right" data-toggle="tooltip" title="Select the language for your materials"></span>
						<select class="form-control" id="language">
							<option value="0">
								English
							</option>
							<option value="1">
								Spanish
							</option>
						</select>
					</div>
					<div class="text-center">
						<button class="bloc-button btn btn-block btn-lg btn-ultramarine-blue" type="submit">
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- bloc-0 END -->

<!-- ScrollToTop Button -->
<a class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1',this)"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32"><path class="scroll-to-top-btn-icon" d="M30,22.656l-14-13-14,13"/></svg></a>
<!-- ScrollToTop Button END-->


<!-- bloc-2 -->
<div class="bloc bgc-black d-bloc" id="bloc-2">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<h4 class="mg-md text-sm-left text-center tc-white">
					About
				</h4><a href="index.php" class="a-btn a-block footer-link">Created by Daniel Messer</a><a href="index.php" class="a-btn a-block">Changelog</a><a href="index.php" class="a-btn a-block footer-link">Email Dan</a>
			</div>
			<div class="col-md-3 col-sm-6">
				<h4 class="mg-md text-sm-left text-center tc-white">
					Other Goodies
				</h4>
				<div class="row">
					<div class="col-6 col-sm-2 col offset-sm-0">
						<div class="text-center">
							<a href="https://blocsapp.com/"><span class="icon-md fa fa-headphones-alt"></span></a>
						</div>
					</div>
					<div class="col-6 col-sm-2">
						<div class="text-center">
							<a href="https://blocsapp.com/"><span class="icon-md fab fa-mastodon"></span></a>
						</div>
					</div>
					<div class="col-6 col-sm-2">
						<div class="text-center">
							<a href="https://blocsapp.com/"><span class="icon-md fab fa-gitlab"></span></a>
						</div>
					</div>
					<div class="col-6 col-sm-2">
						<div class="text-center">
							<a href="https://blocsapp.com/"><span class="icon-md fa fa-beer"></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-2 END -->

</div>
<!-- Main container END -->
    


<!-- Additional JS -->
<script src="./js/jquery.min.js?9449"></script>
<script src="./js/bootstrap.bundle.min.js?9217"></script>
<script src="./js/blocs.min.js?7697"></script>
<script src="./js/jqBootstrapValidation.js"></script>
<script src="./js/formHandler.js?2601"></script>
<script src="./js/lazysizes.min.js" defer></script><!-- Additional JS END -->


</body>
</html>
