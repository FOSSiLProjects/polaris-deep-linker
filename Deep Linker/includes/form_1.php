<?php	
	if (empty($_POST['url']) && strlen($_POST['url']) == 0 || empty($_POST['ctx']) && strlen($_POST['ctx']) == 0)
	{
		return false;
	}
	
	$url = $_POST['url'];
	$ctx = $_POST['ctx'];
	$keywords = $_POST['keywords'];
	$title = $_POST['title'];
	$author = $_POST['author'];
	$email = $_POST['email'];
	$language = $_POST['language'];
	
	$to = 'receiver@yoursite.com'; // Email submissions are sent to this email

	// Create email	
	$email_subject = "Message from a Blocs website.";
	$email_body = "You have received a new message. \n\n".
				  "Url: $url \nCtx: $ctx \nKeywords: $keywords \nTitle: $title \nAuthor: $author \nEmail: $email \nLanguage: $language \n";
	$headers = "MIME-Version: 1.0\r\nContent-type: text/plain; charset=UTF-8\r\n";	
	$headers .= "From: contact@yoursite.com\n";
	$headers .= "Reply-To: DoNotReply@yoursite.com";	
	
	mail($to,$email_subject,$email_body,$headers); // Post message
	return true;			
?>