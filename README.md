# Polaris Deep Linker

A little web app that creates deep links into the Polaris PowerPAC using the Boolean search.

### Make your own deep links

While the PowerPAC offers the ability to use a few different calls with the view.aspx method, I think the Boolean search is a better "hook" into the PowerPAC's search. This little app will help you build a persistent link to materials and results in your Polaris PowerPAC.